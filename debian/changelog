sinntp (1.6-6) unstable; urgency=medium

  * Team upload.
  * d/watch: Fix uscan error.

 -- Colin Watson <cjwatson@debian.org>  Wed, 12 Feb 2025 12:35:02 +0000

sinntp (1.6-5) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

  [ Colin Watson ]
  * Depend on python3-standard-nntplib (closes: #1094719).
  * Add an autopkgtest to ensure that the program can start up and produce
    help output.

 -- Colin Watson <cjwatson@debian.org>  Fri, 31 Jan 2025 10:15:45 +0000

sinntp (1.6-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Check upstream PGP signatures.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 16 Nov 2022 13:08:19 +0000

sinntp (1.6-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 26 May 2022 18:03:09 +0100

sinntp (1.6-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Wed, 20 Apr 2022 20:25:50 -0400

sinntp (1.6-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/tests/tests: sinntp is an application, so only test with the
    default Python 3 version. (Closes: #975518)

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 29 Nov 2020 14:33:36 +0100

sinntp (1.6-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/tests/control: added restriction to fix CI test. Thanks to
    Paul Gevers <elbrus@debian.org>. (Closes: #953908)

 -- Marcelo Soares Mota <motasmarcelo@gmail.com>  Thu, 11 Jun 2020 14:29:16 -0300

sinntp (1.6-1) unstable; urgency=medium

  * Team upload.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field.
  * d/rules: Remove trailing whitespaces.

  [ Mattia Rizzolo ]
  * d/clean: drop now unneeded rules, dh_clean already does that by itself.
  * Bump debhelper compat level to 12, and use debhelper-compat instead of
    the debian/compat file
  * Drop long obsolete debian/pyversions file.
  * d/tests/control: Drop unknown Features: no-build-needed.
  * d/control:
    + Drop Piotr Lewandowski from uploaders at his request to mia@.
    + Bump Standards-Version to 4.4.1:
      - Set Rules-Requires-Root: no.

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 12 Jan 2020 13:45:54 +0100

sinntp (1.6-0.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Convert the autopkgtest to Python 3.
  * Remove the old homepage from debian/copyright.

 -- Adrian Bunk <bunk@debian.org>  Mon, 06 Jan 2020 21:33:26 +0200

sinntp (1.6-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release.
    - Uses Python 3. (Closes: #938489)
  * Updated Homepage and debian/watch. (Closes: #783971, #827146)

 -- Adrian Bunk <bunk@debian.org>  Thu, 26 Dec 2019 08:55:37 +0200

sinntp (1.5-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Build using dh-python. Closes: #786252.

 -- Matthias Klose <doko@debian.org>  Sat, 22 Aug 2015 13:54:35 +0200

sinntp (1.5-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
    + Implement an option to ignore ~/.netrc (closes: #668927). Thanks to Dirk
      Griesbach for the bug report and the initial patch.
  * Drop dependency on python-xdg, no longer used.
  * Bump standards version to 3.9.3.
    + Update debian/copyright URI.
  * Update the copyright file.
  * Use xargs to iterate over all Python versions.
  * Run tests with --verbose.
  * Add DEP-8 tests.

 -- Jakub Wilk <jwilk@debian.org>  Wed, 18 Apr 2012 23:10:23 +0200

sinntp (1.4-1) unstable; urgency=low

  * Team upload.
  * New upstream release (closes: #635613).
  * Bump standards version to 3.9.2 (no changes needed).
  * Update debian/copyright.
  * Run tests at build time. Add python-all to Build-Depends.
  * Bump minimum required version of debhelper to 7.0.50 (for overrides).
  * Remove myself from uploaders.

 -- Jakub Wilk <jwilk@debian.org>  Mon, 15 Aug 2011 23:48:40 +0200

sinntp (1.3.2-1) unstable; urgency=low

  * New upstream release (closes: #585839).

 -- Piotr Lewandowski <piotr.lewandowski@gmail.com>  Thu, 24 Jun 2010 11:24:38 +0200

sinntp (1.3.1-2) unstable; urgency=low

  [ Piotr Lewandowski ]
  * Bump standards version to 3.8.4 (no changes needed).

  [ Jakub Wilk ]
  * Update my e-mail addresses.
  * Switch to source format 3.0 (quilt).
  * Update the watch file.

 -- Jakub Wilk <jwilk@debian.org>  Thu, 13 May 2010 13:21:57 +0200

sinntp (1.3.1-1) unstable; urgency=low

  * New upstream release.
  * debian/control: use simpler URL for Homepage field.

 -- Piotr Lewandowski <piotr.lewandowski@gmail.com>  Wed, 13 Jan 2010 10:48:41 +0000

sinntp (1.3-1) unstable; urgency=low

  * Initial release (closes: #542262).

 -- Piotr Lewandowski <piotr.lewandowski@gmail.com>  Mon, 14 Sep 2009 20:59:12 +0200
